#!/bin/env sh
DEST=public
SRC=src

echo "Recreating $DEST folder"
rm -rf ./$DEST
mkdir -p ./$DEST

echo "Minifying files"
minify -r -o ./$DEST/ ./$SRC
mv ./$DEST/$SRC/* ./$DEST/
rm -rf ./$DEST/$SRC

echo "Copying non minified files"
files=$(find src/* -type f -not -name '*.html' -not -name '*.svg' -not -name '*.css' -not -name '*.js' | sed "s/^src\/\(.*\)\$/\1/g")
for file in $files
do
	echo $file
	cp ./$SRC/$file ./$DEST/$file
done
