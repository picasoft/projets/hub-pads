# Hub des documents collaboratifs Picasoft

Ce portail permet de créer un document collaboratif parmi les outils proposés par Picasoft via une interface unique en un clic.

Il a été fait maison afin d'avoir un outil qui nous correspond exactement. Une version est automatiquement déployée [sur les pages Gitlab](https://picasoft.gitlab.utc.fr/projets/hub-pads/), il s'agit d'une version générée par la chaîne d'intégration et qui est automatiquement minifiée [par cet outil](https://github.com/tdewolff/minify) afin de réduire au maximum la taille finale du site. Vous pouvez manuellement construire le site minifié en lançant le script `build.sh` à la racine, le résultat sera dans le dossier `public`.

Le site a été écrit par Picasoft est est sous licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/fr/).
