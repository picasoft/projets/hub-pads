function randomString(stringLength)
{
	const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	let randomString = '';

	for (let i = 0; i < stringLength; i++)
	{
		const rnum = Math.floor(Math.random() * chars.length);
		randomString += chars.substring(rnum, rnum + 1);
	}
	return randomString;
}

function getURL(service, padName)
{
	let base = '';

	switch (service) {
		case 'week-pad':
			base = 'https://week.pad.picasoft.net/p/';
			break;
		case 'etherpad':
			base = 'https://pad.picasoft.net/p/';
			break;
		case 'codimd':
			base = 'https://md.picasoft.net/';
			break;
		case 'whiteboard':
			base = 'https://board.picasoft.net/app/?whiteboardid=';
			break;
		default:
			console.log(`Sorry, ${service} is an unknown service name :(.`);
			return '';
	}

	if (padName === '') {
		padName = randomString(15);
	}

	return base + padName;
}

function go(service) {
	padName = document.getElementById('pad-name').value;
	anchor = document.getElementById(`anchor-${service}`).href = getURL(service, padName);
}
